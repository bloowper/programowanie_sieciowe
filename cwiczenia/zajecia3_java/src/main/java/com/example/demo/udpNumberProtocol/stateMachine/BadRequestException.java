package com.example.demo.udpNumberProtocol.stateMachine;

public class BadRequestException extends Exception {
    public BadRequestException() {

    }

    public BadRequestException(String m) {
        super(m);
    }
}
