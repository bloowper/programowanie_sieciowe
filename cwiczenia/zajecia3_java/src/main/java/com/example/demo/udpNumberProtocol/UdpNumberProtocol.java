package com.example.demo.udpNumberProtocol;
import com.example.demo.udpNumberProtocol.stateMachine.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class UdpNumberProtocol implements Runnable{
    private static Logger logger = LoggerFactory.getLogger(UdpNumberProtocol.class);

    ProtocolStateMachine protocolStateMachine;
    DatagramSocket socketDatagram;
    DatagramPacket request;
    boolean working;




    InetAddress getInetAddress(){
        return socketDatagram.getInetAddress();
    }

    public UdpNumberProtocol() {
        working = true;
    }

    public ProtocolStateMachine getProtocolStateMachine() {
        return protocolStateMachine;
    }

    public void setProtocolStateMachine(ProtocolStateMachine protocolStateMachine) {
        this.protocolStateMachine = protocolStateMachine;
    }

    public DatagramSocket getSocketDatagram() {
        return socketDatagram;
    }

    public void setSocketDatagram(DatagramSocket socketDatagram) {
        this.socketDatagram = socketDatagram;
    }

    public DatagramPacket getRequest() {
        return request;
    }

    public void setRequest(DatagramPacket request) {
        this.request = request;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }


    /**
     *  wywolywane gdy fasolka jest niszczona
     */
    public void close(){
        if (socketDatagram != null) {
            socketDatagram.close();
        }
        logger.info("Zamknieto socket");
    }

    @Override
    public void run() {
        //TODO: 27.03.2021
        // jak rozwiazac zeby watek z serverem byl prawidlowo zamykany.. moze timeout dla servera np 5 sekund + zmienna bool woorking
        while (working) {
            try {
                logger.info("Oczekiwanie na request");
                try { socketDatagram.receive(request); }
                catch (SocketTimeoutException exception) { continue; }
                if(request.getData()[0] == 88){
                    //NETCAT mi puka 4 krotnie bitem 88
                    //w momencie gdy ma ustawiona flage -v
                    logger.info("Otrzymano request o tresci [88,0....]");
                    continue;
                }
                logger.info("Otrzymano request");
                String requestString = new String(request.getData(), StandardCharsets.UTF_8);
                byte[] response = (protocolStateMachine.processRequest(requestString)+"\r\n").getBytes(StandardCharsets.UTF_8);
                DatagramPacket responseDatagramPacket = new DatagramPacket(response, response.length, request.getAddress(), request.getPort());
                socketDatagram.send(responseDatagramPacket);
                logger.info(String.format("odpowiedz na request: %s",requestString));
                for (int i = 0; i < request.getLength(); i++) {
                    if(request.getData()[i] == 0)break;
                    request.getData()[i]=0;
                }
            } catch (IOException exception) {
                logger.error("Cos poszlo nie tak");
                exception.printStackTrace();
            } catch (BadRequestException exception) {
                logger.info("Otrzymano bledne zapytanie");
                final byte[] bytes = "ERROR\r\n".getBytes(StandardCharsets.UTF_8);
                final DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, request.getAddress(), request.getPort());
                try {
                    socketDatagram.send(datagramPacket);
                    logger.error("odpowiedziano na niepoprawny request");
                } catch (IOException e) {
                    logger.error("Niepowodzenie przy wysylaniu odpowiedzi na niepoprawne zapytanie");
                    e.printStackTrace();
                }
            }
        }
        logger.info("Konczenie pracy watku UdpNumberProtocol");
    }

}
