package com.example.demo.udpNumberProtocol;

import com.example.demo.udpNumberProtocol.stateMachine.NumberProtocolStateMachine;
import com.example.demo.udpNumberProtocol.stateMachine.ProtocolStateMachine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

@Configuration
@PropertySource("classpath:server.properties")
@ComponentScan(basePackages = {"com.example.demo.udpNumberProtocol","com.example.demo.udpNumberProtocol.stateMachine"})
public class UdpNumberProtocolConfig {
    private static Logger logger = LoggerFactory.getLogger(UdpNumberProtocolConfig.class);

    @Value("${server.datagramSize}")
    private int serverDatagramSize;
    @Value("${server.port}")
    private int serverPort;
    @Value("${server.timeout}")
    private int serverTimeOut;



    @Bean
    public ProtocolStateMachine protocolStateMachine() {
        ProtocolStateMachine stateMachine = new NumberProtocolStateMachine();
        return stateMachine;
    }

    @Bean
    public DatagramSocket datagramSocket() throws SocketException {
        DatagramSocket socket;
        try {
            socket = new DatagramSocket(serverPort);
            socket.setSoTimeout(serverTimeOut);
        } catch (SocketException e) {
            logger.error(String.format("DatagramSocket bean cannot be created on port %d!",serverPort), e);
            throw e;
        }
        return socket;
    }

    @Bean()
    public DatagramPacket request() {
        final DatagramPacket datagramPacket = new DatagramPacket(new byte[serverDatagramSize],serverDatagramSize);
        return datagramPacket;
    }



    @Bean(destroyMethod = "close")
    public UdpNumberProtocol udpNumberProtocol() {
        UdpNumberProtocol udpNumberProtocol = new UdpNumberProtocol();
        try {
            udpNumberProtocol.setProtocolStateMachine(protocolStateMachine());
            udpNumberProtocol.setSocketDatagram(datagramSocket());
            udpNumberProtocol.setRequest(request());
        } catch (SocketException e) {
            throw new BeanCreationException("cannot create udpNumberProtocol bean");
        }
        return udpNumberProtocol;
    }
}
