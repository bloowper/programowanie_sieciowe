package com.example.demo.udpNumberProtocol.stateMachine;

class OverflowRequestException extends BadRequestException {
    public OverflowRequestException() {
    }

    public OverflowRequestException(String message) {
        super(message);
    }
}
