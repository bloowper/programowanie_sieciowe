package com.example.demo;

import com.example.demo.udpNumberProtocol.UdpNumberProtocol;
import com.example.demo.udpNumberProtocol.UdpNumberProtocolConfig;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.slf4j.LoggerFactory;
import  org.slf4j.Logger;
import sun.misc.Signal;


@SpringBootApplication
public class DemoServer implements CommandLineRunner {

    private static Logger logger = LoggerFactory.getLogger(DemoServer.class);
    private static AnnotationConfigApplicationContext context;


    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DemoServer.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        context = new AnnotationConfigApplicationContext(UdpNumberProtocolConfig.class);
        UdpNumberProtocol numberProtocol = context.getBean(UdpNumberProtocol.class);
        Signal.handle(new Signal("INT"), sig -> numberProtocol.setWorking(false));
        final Thread serverThread = new Thread(numberProtocol);
        serverThread.start();
        serverThread.join();
    }
}
