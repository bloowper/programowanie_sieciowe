package com.example.demo.udpNumberProtocol.stateMachine;

/**
 *  Klasa abstrakcyjna do przetwarzania zapytania od klienta
 *
 */
public abstract class ProtocolStateMachine {
    protected state currentState;
    protected state previousState;
    protected String errorMessage;
    public ProtocolStateMachine() {
        currentState = state.IS;
        previousState = state.IS;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @throws BadRequestException kiedy zapytanie jest nieprawidlowe
     */
    abstract void changeState() throws BadRequestException;

    public abstract String processRequest(String request) throws BadRequestException;
}

enum state{
    IS,
    C,
    S,
    OK,
    ERR
}

