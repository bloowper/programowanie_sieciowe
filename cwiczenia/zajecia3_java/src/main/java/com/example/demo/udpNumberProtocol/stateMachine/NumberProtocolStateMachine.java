package com.example.demo.udpNumberProtocol.stateMachine;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public class NumberProtocolStateMachine
        extends ProtocolStateMachine {
    final static Logger logger = LoggerFactory.getLogger(NumberProtocolStateMachine.class);
    char aChar;
    int tempValue;
    int wholeValue;

    @Override
    void changeState() throws BadRequestException {
        //TODO
        // zamknac klase przed mozliwoscia uzywanie metody na zewnatrz pakietu
        if (aChar == ' ') {
            // *->S
            if (currentState == state.C) {
                //C->S
                wholeValue += tempValue;
                tempValue =0;
                previousState = currentState;
                currentState = state.S;
                return;
            }
            if (currentState == state.S) {
                //S->S
                previousState = currentState;
                currentState = state.S;
                return;
            }
            if (currentState == state.IS) {
                //IS->IS
                previousState = currentState;
                currentState = state.IS;
                return;
            }
        }
        if (Character.isDigit(aChar)) {
            // *->C
            if (currentState == state.C) {
                // C->C
                tempValue = tempValue*10 + Character.getNumericValue(aChar);
                previousState = currentState;
                currentState = state.C;
                return;
            }
            if (currentState == state.S) {
                // S->C
                tempValue = Character.getNumericValue(aChar);
                previousState = currentState;
                currentState = state.C;
                return;
            }
            if (currentState == state.IS) {
                // IS->C
                tempValue = Character.getNumericValue(aChar);
                previousState = currentState;
                currentState = state.C;
                return;
            }
        }
        if (aChar == '\n') {
            // *->OK
            if (currentState == state.S) {
                // s->ok
                previousState = currentState;
                currentState = state.OK;
                return;
            }
            if (currentState == state.C) {
                // c->ok
                wholeValue += tempValue;
                tempValue = 0;
                previousState = currentState;
                currentState = state.OK;
                return;
            }
        }
        throw new BadRequestException("Zapytnie bylo nie prawidlowe");
    }

    @Override
    public String processRequest(String request) throws BadRequestException {
        logger.info(request);
        for (int i = 0; i < request.length(); i++) {
            aChar = request.charAt(i);
            if(aChar == '\n' || aChar=='\r' || aChar == 0){
                break;
            }
            changeState();
            chechOverflow();
        }
        aChar = '\n';
        changeState();
        chechOverflow();

        var toReturn = String.valueOf(wholeValue);
        tempValue = 0;
        wholeValue = 0;
        currentState = state.IS;
        previousState = state.IS;
        return toReturn;
    }

    void chechOverflow() throws BadRequestException {
         // W tej definicji protokolu overflow mozna rozpoznac przy tym ze suma bedzie mniejza od 0, bo protokol nie dopuszcza takich wartosci / sum
        if(tempValue<0){
            throw new BadRequestException("Overflow");
        }
        if(wholeValue<0){
            throw new BadRequestException("Overflow");
        }
    }
}