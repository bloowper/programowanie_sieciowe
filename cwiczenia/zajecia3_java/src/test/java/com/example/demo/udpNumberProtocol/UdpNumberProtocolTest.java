package com.example.demo.udpNumberProtocol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.lang.Thread;
import static org.junit.jupiter.api.Assertions.*;

class UdpNumberProtocolTest {
    final static Logger logger = LoggerFactory.getLogger(UdpNumberProtocolTest.class);
    private UdpNumberProtocol udp;
    private GenericApplicationContext ctx;
    private Properties serverProperties;
    private DatagramPacket response;
    private DatagramSocket socket;
    private Thread serverThread;


    @BeforeEach
    void setUp() throws IOException {
        ctx = new AnnotationConfigApplicationContext(UdpNumberProtocolConfig.class);
        udp = ctx.getBean(UdpNumberProtocol.class);
        assertNotNull(udp);
        assertNotNull(udp.getSocketDatagram());
        response = new DatagramPacket(new byte[1024], 1024);
        socket = new DatagramSocket(0);
        socket.setSoTimeout(10000);
        serverThread = new Thread(udp);
    }

    @Test
    void run() throws InterruptedException, IOException {
        //TODO: 27.03.2021
        // port dla testowania endpointu pobierac z server.propertie
        // hardcodowanie nie jest dobrym pomyslem

        serverThread.start();
        Thread.sleep(500);
        Map<String,String> requestResponseMap = new HashMap<>();
        requestResponseMap.put("4","4\r\n");
        requestResponseMap.put("5 ","5\r\n");
        requestResponseMap.put("6 6","12\r\n");
        requestResponseMap.put("8","8\r\n");
        requestResponseMap.put("12 12 12","36\r\n");
        requestResponseMap.put("1 1 1 1 1 1 1 1 1 1","10\r\n");
        requestResponseMap.put("1","1\r\n");
        requestResponseMap.put("100 100 100 100 100 100 100 100 100 100", "1000\r\n");
        requestResponseMap.put("12","12\r\n");
        //
        requestResponseMap.put(" 4","4\r\n");
        requestResponseMap.put(" 5 ","5\r\n");
        requestResponseMap.put(" 6 6","12\r\n");
        requestResponseMap.put(" 8","8\r\n");
        requestResponseMap.put(" 12 12 12","36\r\n");
        requestResponseMap.put(" 1 1 1 1 1 1 1 1 1 1","10\r\n");
        requestResponseMap.put(" 1","1\r\n");
        requestResponseMap.put(" 100 100 100 100 100 100 100 100 100 100", "1000\r\n");
        requestResponseMap.put(" 12","12\r\n");
        //
        requestResponseMap.put("  4","4\r\n");
        requestResponseMap.put("  5 ","5\r\n");
        requestResponseMap.put("  6 6","12\r\n");
        requestResponseMap.put("  8","8\r\n");
        requestResponseMap.put("  12 12 12","36\r\n");
        requestResponseMap.put("  1 1 1 1 1 1 1 1 1 1","10\r\n");
        requestResponseMap.put("  1","1\r\n");
        requestResponseMap.put("  100 100 100 100 100 100 100 100 100 100", "1000\r\n");
        requestResponseMap.put("  12","12\r\n");
        //
        requestResponseMap.put("  4 ","4\r\n");
        requestResponseMap.put("  5  ","5\r\n");
        requestResponseMap.put("  6   6 ","12\r\n");
        requestResponseMap.put("  8 ","8\r\n");
        requestResponseMap.put("  12   12  12 ","36\r\n");
        requestResponseMap.put("  1  1 1 1 1 1    1  1  1 1 ","10\r\n");
        requestResponseMap.put("  1 ","1\r\n");
        requestResponseMap.put("  100  100 100 100 100 100 100 100 100 100 ", "1000\r\n");
        requestResponseMap.put("  12    ","12\r\n");

        requestResponseMap.forEach((req, res) -> {
            try {
                final byte[] reqBytes = req.getBytes(StandardCharsets.UTF_8);
                final DatagramPacket reqDatagram = new DatagramPacket(reqBytes, reqBytes.length, InetAddress.getLocalHost(), 2020);
                socket.send(reqDatagram);
                socket.receive(response);
                final String responseString = new String(response.getData(), 0, response.getLength(), StandardCharsets.UTF_8);
                assertEquals(res,responseString);
                Thread.sleep(20);
            } catch (Exception e) {
                logger.error(String.format("%s -> %s",req,req));
                e.printStackTrace();
            }
        });

        requestResponseMap.clear();
        requestResponseMap.put("1 -1","ERROR\r\n");
        requestResponseMap.put("-1 -1","ERROR\r\n");
        requestResponseMap.put("-1 -1","ERROR\r\n");
        requestResponseMap.put("adsf","ERROR\r\n");
        requestResponseMap.put("1,","ERROR\r\n");
        requestResponseMap.put("1111111111111/1","ERROR\r\n");
        requestResponseMap.put("asdf","ERROR\r\n");
        requestResponseMap.put("999999999999","ERROR\r\n");

        requestResponseMap.forEach((req, res) -> {
            try {
                final byte[] reqBytes = req.getBytes(StandardCharsets.UTF_8);
                final DatagramPacket reqDatagram = new DatagramPacket(reqBytes, reqBytes.length, InetAddress.getLocalHost(), 2020);
                socket.send(reqDatagram);
                socket.receive(response);
                final String responseString = new String(response.getData(), 0, response.getLength(), StandardCharsets.UTF_8);
                assertEquals(res,responseString);
                Thread.sleep(20);
            } catch (Exception e) {
                logger.error(String.format("%s -> %s",req,req));
                e.printStackTrace();
            }
        });

        udp.setWorking(false);
        serverThread.join();

        // nie wiem jak mam zakonczyc ten watek... jest w trybie oczekiwania...
    }
}