package com.example.demo.udpNumberProtocol.stateMachine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberProtocolStateMachineTest {


    ProtocolStateMachine stateMachine;

    @BeforeEach
    void init() {
        stateMachine = new NumberProtocolStateMachine();
    }

    @Test
    void processRequest() throws BadRequestException {
        String temp = null;
        temp = stateMachine.processRequest("11 22");
        assertEquals("33",temp);

        temp = stateMachine.processRequest("11 22 ");
        assertEquals("33",temp);

        temp = stateMachine.processRequest(" 11 22");
        assertEquals("33",temp);

        temp = stateMachine.processRequest("  11 22");
        assertEquals("33",temp);

        temp = stateMachine.processRequest("  11 22  ");
        assertEquals("33",temp);

        temp = stateMachine.processRequest("  11   22  ");
        assertEquals("33",temp);
        ////
        temp = stateMachine.processRequest("1 1 2 10");
        assertEquals("14",temp);

        temp = stateMachine.processRequest(" 1 1 2 10");
        assertEquals("14",temp);

        temp = stateMachine.processRequest(" 1   1  2   10  ");
        assertEquals("14",temp);
        ////
        temp = stateMachine.processRequest("1 1 1 1 1 1 1 1 1 1");
        assertEquals("10",temp);

        temp = stateMachine.processRequest("1      1 1 1 1 1 1 1 1 1    ");
        assertEquals("10",temp);

        temp = stateMachine.processRequest("   1 1 1 1 1 1 1 1 1 1");
        assertEquals("10",temp);

        temp = stateMachine.processRequest("    10         10");
        assertEquals("20",temp);

        temp = stateMachine.processRequest("1");
        assertEquals("1",temp);

        temp = stateMachine.processRequest(" 1");
        assertEquals("1",temp);

        temp = stateMachine.processRequest("1 ");
        assertEquals("1",temp);

        temp = stateMachine.processRequest(" 1 ");
        assertEquals("1",temp);

        temp = stateMachine.processRequest("999999999");
        assertEquals("999999999",temp);
    }


    @Test
    void processRequestExceptions(){
        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("111 1,");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("111 1 +12");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("111 1 123123123\t");

        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("   111 1 -12");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("   111 1 -12 asdf");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("fdajfowerji");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("   111 1 +12");
        });

        assertThrows(BadRequestException.class, () -> {
            stateMachine.processRequest("53453453459023459432590543\t\r");
        });
    }
}