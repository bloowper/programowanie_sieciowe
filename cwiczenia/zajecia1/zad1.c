
#include <stdio.h>
#include <malloc.h>

int main(int argc, char *argv[]){

    int *tablica;
    int rozmiar_tablicy = 50;
    tablica = malloc(sizeof(int)*rozmiar_tablicy);

    scanf("%d",tablica);

    for (int i = 0; i < rozmiar_tablicy; ++i) {
        scanf("%d",tablica+i);
        if(*(tablica+i)==0) break;
    }

    for (int i = 0; i < rozmiar_tablicy; ++i) {
        if(*(tablica+i)>10 && *(tablica+i)<100) printf("%d ",*(tablica+i));
    }
    printf("\n");

    free(tablica);
    return 0;
}