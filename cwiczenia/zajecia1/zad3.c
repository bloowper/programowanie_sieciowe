#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>

/*
 * Opracuj funkcję testującą czy przekazany jej bufor zawiera tylko i wyłącznie drukowalne znaki ASCII,
 * tzn. bajty o wartościach z przedziału domkniętego [32, 126]. Funkcja ma mieć następującą sygnaturę:
 * bool drukowalne(const void * buf, int len).
 * Pamiętaj o włączeniu nagłówka <stdbool.h>, bez niego kompilator nie rozpozna ani nazwy typu bool,
 * ani nazw stałych true i false.
 * Zaimplementuj dwa warianty tej funkcji, w pierwszym wariancie funkcja ma pobierać kolejne bajty
 * z bufora przy pomocy operacji indeksowania tablic, w drugim ma używać wskaźnika przesuwającego się z bajtu na bajt.
 */
bool drukowalne1(const void * buf, int len);

bool drukowalne2(const void * buff, int len);

int main(int argc, char *argv[]){
    char* test = "test tes tes tes tes\0";
    bool b = drukowalne1(test, sizeof(test) / sizeof(test[0]));
    printf("%d",b);
}

bool drukowalne1(const void *buf, int len) {
    unsigned char *string = (unsigned char *) buf;
    char *pointer = string;
    int offset = 0;
    // [32, 126]
    while ((*pointer)>=32 && (*pointer)<= 126 && offset<len)
    {
        pointer = (string+offset);
        offset+=1;
    }

    if(offset+1 == len)
        return true;
    return false;
}
