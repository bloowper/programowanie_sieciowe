//
// Created by tomek on 20.03.2021.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libsync.h>
#include <signal.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <math.h>
#include <stdint.h>


int main(){

    int liczbyDoDodania[6]={0};
    int headLiczbDoDodania = -1;
    char *string = "1 +1 1 1   ";

    //    int maxLenghtOfNumber = (int)(log10(INTMAX_MAX)+1)-1;//prevencyjnie obcinam
    int maxLenghtOfNumber = 9;//nie wiem dlaczego ale z mojego debugowania wynika ze to max dlugosc
    char substring[maxLenghtOfNumber + 2];
    int rozmiar = (int)strlen(string);
    bool minus = false;
    bool error = false;
    bool new = true;
    int startOfN = -1;
    int endOfN = -1;

    for (int i = 0; i < rozmiar; ++i) {
        char temp = string[i];
        if(!isascii(temp) || (!isdigit(temp) && !isblank(temp) && temp!='-' && temp!='+' && temp!='\r' && temp!='\n')){
            printf("zapytanie niezgodne z standardem\n");
            error = true;
            break;
        }
        if (temp == '\n' || temp == '\r' || i==rozmiar-1){
            if(!new){
                //jakos przetworzyc i dodac do stosu
                if(isdigit(temp)){
                    endOfN = i;
                }
                if(endOfN - startOfN+1 > maxLenghtOfNumber){
                    error=true;
                    break;
                }
                new = true;
                strncpy(substring,string+startOfN,endOfN-startOfN+1);
                substring[endOfN-startOfN+1]='\0';
                printf("%s\n",substring);
                int converted = atoi(substring);
                if(minus){
                    liczbyDoDodania[headLiczbDoDodania+1] = -converted;
                    headLiczbDoDodania++;
                } else{
                    liczbyDoDodania[headLiczbDoDodania+1] = converted;
                    headLiczbDoDodania++;
                }
            }
            printf("****\n");
            break;
        }
        if (isblank(temp)){
            if(!new){
                if(endOfN<startOfN){
                    endOfN = i-1;
                }
                //jakos przetworzyc i dodac do stosu
                new = true;
                if(endOfN-startOfN+1 > maxLenghtOfNumber){
                    error = true;
                    break;
                }
                strncpy(substring,string+startOfN,endOfN-startOfN+1);
                substring[endOfN-startOfN+1]='\0';
                printf("%s\n",substring);
                int converted = atoi(substring);
                if(minus){
                    liczbyDoDodania[headLiczbDoDodania+1] = -converted;
                    headLiczbDoDodania++;
                } else{
                    liczbyDoDodania[headLiczbDoDodania+1] = converted;
                    headLiczbDoDodania++;
                }
            }
            printf("****\n");
            minus = false;
            continue;
        }
        if(temp == '-') {
            printf("znak : %c\n", temp);
            minus = true;
            continue;
        }
        if (temp == '+') {
            minus = false;
            printf("znak : %c\n",temp);
            continue;
        }

        if(isdigit(temp)){
            printf("num : %c\n", temp);
            if(new){
                new = false;
                startOfN = i;
            } else{
                endOfN = i;
            }
            continue;
        }

    }

    if(error){
        printf("zapytanie bylo nie poprawne\n");

    }

    printf("------------\n");
    for(int i=0 ;i<6;i++){
        printf("%d \n", liczbyDoDodania[i]);
    }//

    return 0;
}
