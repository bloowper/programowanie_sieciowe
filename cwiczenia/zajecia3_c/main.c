#include "main.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#define UNUSED(x) (void)(x)
#define maxLenghtOfNumber 9
#define maxLenghtOfresponse 11
#define bufferSize  1024
//


int liczbyDoDodania[bufferSize];
int headLiczbDoDodania=-1;
char substring[maxLenghtOfNumber];
char resposne[maxLenghtOfresponse];

/*
 * arg[1] - port
 */
int main(int argc, char *argv[]){
    //jakies initial zachowania
    arg_handler(argc,argv);
    unsigned intPort = atoi(argv[1]);
    printf("Port : %d\n",intPort);

    //definicje potrzebnych danych
    int sockfd;
    char buffer[bufferSize];
    struct  sockaddr_in serverAddr, clientAddr;
    memset(&serverAddr,0,sizeof(serverAddr));


//

    //GDY SOCK_STREAM to recfrom nie oczekuje na dane????

    sockfd = socket(AF_INET, SOCK_DGRAM, 0) ;
    if(sockfd<0){
        perror("socket() failed");
        exit(EXIT_FAILURE);
    }

    //inicjalizacje potrzenych danych
    //inicjalizacja API gniazdek: adresy rodziny INET
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(intPort);

    if(bind(sockfd, (struct sockaddr *) &serverAddr, sizeof (serverAddr)) == -1){
        perror("bind() failed");
        exit(EXIT_FAILURE);
    }

    while (1){
        printf("Oczekiwanie na dane\n");
        size_t clientLen = sizeof clientAddr;//to tutaj musi zostac :)
        memset(&clientAddr,0,sizeof (clientAddr));// nie przeszkadza ale nie konieczne
        ssize_t recLen = recvfrom(sockfd,
                                  buffer,
                                  bufferSize,
                                  0,
                                  (struct sockaddr *) &clientAddr,
                                  (socklen_t *) &clientLen);
        if(recLen == -1){
            perror("recfrom() failed");
            exit(EXIT_FAILURE);
        }

        // w c chyba nie ma regexa, wiec trzeba do tego podejść jakoś klasycznie
        //
        bool minus = false;
        bool error = false;
        bool new = true;
        int startOfN = -1;
        int endOfN = -1;
        for (int i = 0; i < recLen; ++i) {
            char temp = buffer[i];
            if(!isascii(temp) || (!isdigit(temp) && !isblank(temp) && temp!='-' && temp!='+' && temp!='\r' && temp!='\n')){
                error = true;
                break;
            }
            if (temp == '\n' || temp == '\r' || i==recLen-1){
                if(!new){
                    //jakos przetworzyc i dodac do stosu
                    if(endOfN<startOfN){
                        endOfN = i-1;
                    }
                    if(isdigit(temp)){
                        endOfN = i;
                    }
                    if(endOfN - startOfN+1 > maxLenghtOfNumber){
                        error=true;
                        break;
                    }
                    new = true;
                    strncpy(substring,buffer+startOfN,endOfN-startOfN+1);
                    substring[endOfN-startOfN+1]='\0';
                    int converted = atoi(substring);
                    if(minus){
                        liczbyDoDodania[headLiczbDoDodania+1] = -converted;
                        headLiczbDoDodania++;
                    } else{
                        liczbyDoDodania[headLiczbDoDodania+1] = converted;
                        headLiczbDoDodania++;
                    }
                }
                break;
            }
            if (isblank(temp)){
                if(!new){
                    if(endOfN<startOfN){
                        endOfN = i-1;
                    }
                    new = true;
                    if(endOfN-startOfN+1 > maxLenghtOfNumber){
                        error = true;
                        break;
                    }
                    strncpy(substring,buffer+startOfN,endOfN-startOfN+1);
                    substring[endOfN-startOfN+1]='\0';
                    int converted = atoi(substring);
                    if(minus){
                        liczbyDoDodania[headLiczbDoDodania+1] = -converted;
                        headLiczbDoDodania++;
                    } else{
                        liczbyDoDodania[headLiczbDoDodania+1] = converted;
                        headLiczbDoDodania++;
                    }
                }
                minus = false;
                continue;
            }
            if(temp == '-') {
                minus = true;
                continue;
            }
            if (temp == '+') {
                continue;
            }
            if(isdigit(temp)){
                if(new){
                    new = false;
                    startOfN = i;
                } else{
                    endOfN = i;
                }
                continue;
            }
        }
        bool overflow=false;
        if(error){
            printf("Zapytanie od .... bylo nieprawidlowe\n");
            sprintf((char *) &resposne, "ERROR");
        } else{
            int toReturn=0;
            for (int i = 0; i <= headLiczbDoDodania; i++) {
                int temp = toReturn + liczbyDoDodania[i];
                if(toReturn>=0 && liczbyDoDodania[i]>=0 && temp < 0){
                        overflow = true;
                        break;
                }
                if(toReturn<0 && liczbyDoDodania[i]<0 && temp > 0){
                    overflow = true;
                    break;
                }
                toReturn =temp;
            }
            if(!overflow)
                sprintf((char *) &resposne, "%d", toReturn);
            if(overflow)
                sprintf((char *) &resposne, "ERROR");
        }
        headLiczbDoDodania = -1;

        int return_value = sendto(sockfd,
                                  resposne,
                                  strlen(resposne),
                                  0,//nie musi byc tej flag/ ciekawe do czego byla tamta flaga w ksiazce
                                  (const struct sockaddr *) &clientAddr,
                                  clientLen);//to moze sie zakonczyc porazka! sprawdzic blad
        if (return_value == -1) {
            perror("sendto() failed");
            exit(EXIT_FAILURE);
        }
    }


    return 0;
}



void arg_handler(int argc, char *argv[]){
    UNUSED(argv);
    if(argc!=2){
        printf("argv[1] - port\n");
        exit(EXIT_FAILURE);
    }
}


