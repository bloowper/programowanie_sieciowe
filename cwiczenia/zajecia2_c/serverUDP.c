//
// Created by tomek on 14.03.2021.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libsync.h>
#include <signal.h>

void sig_handler(int signum);
void arg_handler(int argc, char *argv[]);

#define bufferSize  1024
char *stopkaPowitalna = "Hello world";

/*
 * arg[1] - port
 */
int main(int argc, char *argv[]){
    //jakies initial zachowania
    signal(SIGINT,signal);
    arg_handler(argc,argv);
    unsigned intPort = atoi(argv[1]);
    printf("Port : %d\n",intPort);

    //definicje potrzebnych danych
    int sockfd;
    char buffer[bufferSize];
    struct  sockaddr_in serverAddr, clientAddr;
    memset(&serverAddr,0,sizeof(serverAddr));




    //GDY SOCK_STREAM to recfrom nie oczekuje na dane????

    sockfd = socket(AF_INET, SOCK_DGRAM, 0) ;
    if(sockfd<0){
        perror("socket() failed");
        exit(EXIT_FAILURE);
    }

    //inicjalizacje potrzenych danych
    //inicjalizacja API gniazdek: adresy rodziny INET
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(intPort);

    if(bind(sockfd, (struct sockaddr *) &serverAddr, sizeof (serverAddr)) == -1){
        perror("bind() failed");
        exit(EXIT_FAILURE);
    }

    while (1){
        printf("Oczekiwanie na dane\n");
        size_t clientLen = sizeof clientAddr;//to tutaj musi zostac :)
        memset(&clientAddr,0,sizeof (clientAddr));// nie przeszkadza ale nie konieczne
        ssize_t recLen = recvfrom(sockfd,
                                  buffer,
                                  bufferSize,
                                  0,
                                  (struct sockaddr *) &clientAddr,
                                  &clientLen);
        if(recLen == -1){
            perror("recfrom() failed");
            exit(EXIT_FAILURE);
        }
        printf("otrzymano : %s\n",buffer);//odradzane!
        int return_value = sendto(sockfd,
                                stopkaPowitalna,
                                strlen(stopkaPowitalna),
                                0,//nie musi byc tej flagi!
                                &clientAddr,
                                clientLen);//to moze sie zakonczyc porazka
        if (return_value == -1) {
            perror("sendto() failed");
            exit(EXIT_FAILURE);
        }
    }


    return 0;
}


void sig_handler(int signum){
    if(signum == SIGINT){
        exit(EXIT_SUCCESS);
    }
}


void arg_handler(int argc, char *argv[]){

}
