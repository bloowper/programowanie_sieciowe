//
// Created by tomek on 10.03.2021.
//
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libsync.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFSIZE 1024
char buff[BUFFSIZE];
bool work = true;

void arg_handler(int argc, char *argv[]);


int main(int argc, char *argv[]){
    char *charIP = argv[1];
    int portInt = atoi(argv[2]);
    printf("ip : %s\n",charIP);
    printf("port : %d\n",portInt);
    int socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if(socketDescriptor<0){
        perror("socket() failder");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in sockaddrCli;
    memset(&sockaddrCli,0,sizeof(sockaddrCli));//dlaczego ta funkcja jest w string.h a nie w czyms wzwiazanym z pamiecia... bez sensu...
    sockaddrCli.sin_family = AF_INET;
    sockaddrCli.sin_addr.s_addr = inet_addr(charIP);
    sockaddrCli.sin_port = htons(portInt);


    if(connect(socketDescriptor, (struct sockaddr *) &sockaddrCli, sizeof(sockaddrCli)) == -1){
        perror("connect() failed");
        exit(EXIT_FAILURE);
    }
    printf("Nawiazano polaczenie z serwerem\n");

    sleep(2);
//    while (work) {
//        ssize_t nOfReadBytes = read(socketDescriptor, buff, BUFFSIZE);
//        printf("Wczytano %d bajtow\n", nOfReadBytes);
//        printf("%s\n", buff);
//    }
    printf("Przed read()\n");
    ssize_t nOfReadBytes = read(socketDescriptor, buff, BUFFSIZE);
    printf("Wczytano %d bajtow\n", nOfReadBytes);
    printf("%s\n", buff);

    close(socketDescriptor); // wydaje mi sie ze tak sie powinno : ale czy tam jest

    return 0;
}


void arg_handler(int argc, char *argv[]){
    if(argc!=3){
        printf("wymagane argumenty 1:ip 2:port\n");
        exit(EXIT_FAILURE);
    }
}