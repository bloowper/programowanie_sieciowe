//
// Created by tomek on 14.03.2021.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libsync.h>
#include <signal.h>
#define bufferSize  1024
void sig_handler(int signum);
void arg_handler(int argc, char *argv[]);

int main(int argc, char *argv[]){
    arg_handler(argc,argv);
    signal(SIGINT,sig_handler);

    int sockedFD;
    char buffer[bufferSize];
    struct sockaddr_in servaddr;
    int portInt = atoi(argv[2]);
    sockedFD = socket(AF_INET,SOCK_DGRAM,0);
    if (sockedFD == -1){
        perror("socket() failed :");
        exit(EXIT_FAILURE);
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(portInt);



    return 0;
}


void sig_handler(int signum){
    if(signum == SIGINT) {
        exit(EXIT_SUCCESS);
    }
}

void arg_handler(int argc, char *argv[]){
    if(argc != 3){
        printf("argv[1] - adres \n argv[2] - port\n");
        exit(EXIT_FAILURE);
    }
}