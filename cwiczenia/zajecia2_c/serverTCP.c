//
// Created by tomek on 08.03.2021.
//
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libsync.h>
#include <signal.h>

#define bufferSize  1024
bool work = true;
char sendBuff[bufferSize];

//TODO 1
// ogarnac dlaczego to nie przechwytuje sygnalu....
void sig_handler(int signum);
void arg_handler(int argc, char *argv[]);

int main(int argc, char *argv[]){
    arg_handler(argc,argv);
    signal(SIGINT, sig_handler);//TODO 1

    unsigned int intPort = atoi(argv[1]);
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in adres;
    memset(&adres,0,sizeof(adres));
    adres.sin_family = AF_INET;
    adres.sin_port = htons(intPort);
    if (bind(fd, (struct sockaddr *) &adres, sizeof(adres)) == -1) {
        perror("bind() sie nie powiodl");
        exit(EXIT_FAILURE);
    }
    listen(fd,5);//moze oczekiwac 5 klientow na polaczenie

    int numer_polaczenia = 0;
    while (work){
        printf("Oczekiwania na klienta \n");
        int acceptedfd = accept(fd, NULL, 0);
        if(acceptedfd<0){
            perror("accepty() failed\n");
            exit(EXIT_FAILURE);
        }
        printf("Klient zostal podlaczony\n");
        char string[] = "Hello world";
//        snprintf(sendBuff,bufferSize,"Hello world nr %d",numer_polaczenia++);
        write(acceptedfd,string,sizeof (string));
        close(acceptedfd);
    }
    return 0;
}


void sig_handler(int signum){
    if(signum == SIGINT){
        printf("Obsluga ctr + c\n");
        work = false;
        exit(EXIT_SUCCESS);
    }
}

void arg_handler(int argc, char *argv[]){
    if(argc!=2){
        printf("wymagane argumenty 1: port\n");
        exit(EXIT_FAILURE);
    }
}