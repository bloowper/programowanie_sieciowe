package socket_programing.simple;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;



public class ServerMain {
    private static Logger logger = LoggerFactory.getLogger(ServerMain.class);
    private static final int PORT = 1025;

    public static void main(String[] args) {
        try(DatagramSocket socket = new DatagramSocket(PORT))      {
            while (true) {
                DatagramPacket request = new DatagramPacket(new byte[1024], 1024);
                logger.info("Oczekiwanie na request");
                socket.receive(request);//blocking method
                logger.info("Otrzymano request");
                byte[] data = "Hello world".getBytes(StandardCharsets.US_ASCII);//
                DatagramPacket response = new DatagramPacket(data, data.length, request.getAddress(), request.getPort());
                socket.send(response);
                logger.info("odpowiedziano na request");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
