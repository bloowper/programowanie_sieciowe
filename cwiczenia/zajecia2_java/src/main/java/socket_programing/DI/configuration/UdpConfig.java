package socket_programing.DI.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:/client.properties", "classpath:/datagram.properties", "classpath:/server.properties"})
@ComponentScan(basePackages = "socket_programing")
public class UdpConfig {

    // Client properties
    @Value("${client.port}") private String clientPort;
    @Value("${client.ipServer}") private String clientIpToServer;
    // Server properties
    @Value("${server.porter}") private String serverPort;
    // Datagram properties
    @Value("${datagram.size}") private int bufforSize;



}
