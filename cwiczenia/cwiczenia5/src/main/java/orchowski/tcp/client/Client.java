package orchowski.tcp.client;

import orchowski.tcp.stateMachine.StateMachine;

import java.net.Socket;

public interface Client extends Runnable {
    @Override
    void run();

    boolean isWorking();

    void setWorking(boolean working);

    void setStateMachine( StateMachine stateMachine);

    StateMachine getStateMachine();

    void close();

    void afterPropertySet();

    Socket getSocket();

    void setSocket(Socket socket);
}
