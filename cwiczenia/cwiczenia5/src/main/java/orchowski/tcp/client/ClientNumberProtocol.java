package orchowski.tcp.client;
import lombok.ToString;
import orchowski.tcp.stateMachine.StateMachine;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.BeanCreationException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;


public class ClientNumberProtocol implements Client {
    @Setter @Getter StateMachine stateMachine;
    @Setter @Getter Socket socket;
    @Setter @Getter boolean working;
    @Setter @Getter int bufferSize;
    private InputStreamReader inputStream = null;
    private OutputStreamWriter outputStream = null;
    private final Logger logger = LoggerFactory.getLogger(ClientNumberProtocol.class);

    public ClientNumberProtocol() throws SocketException {
        working=true;
    }


    @Override
    public void run() {
        logger.info(String.format("local port : %s | host start",socket.getPort()));
        var chars = new char[bufferSize];

        try {
            inputStream = new InputStreamReader(socket.getInputStream(), StandardCharsets.US_ASCII);
            outputStream = new OutputStreamWriter(socket.getOutputStream(),StandardCharsets.US_ASCII);
        } catch (IOException exception) {
            exception.printStackTrace();
            logger.error("socket.getinputstream error");
            return;
        }
        while (working && socket.isConnected()){
            try {
                logger.debug(String.format("local port : %s | reading data",socket.getPort()));
                var nof = inputStream.read(chars);
                if(nof == -1) {
                    logger.debug("Host disconected");
                    break;
                }
                var charsTruncated = Arrays.copyOf(chars, nof);
                stateMachine.processRequest(charsTruncated);
                var response = stateMachine.getResponse();
                outputStream.write(response);
                outputStream.flush();
            } catch (IOException exception) {
                exception.printStackTrace();
                break;
            }
        }
        logger.info(String.format("local port : %s | host stop",socket.getPort()));
        close();
    }

    @Override
    public void close() {
        logger.debug(String.format("local port : %s | closing method",socket.getPort()));
        if (socket!=null && !socket.isClosed()) {
            try {
                inputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException exception) {
                logger.error("Error occurred when closing socket : "+socket.getPort());
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void afterPropertySet() {
        if(stateMachine == null) {
            logger.error(this.getClass().getName()+" must have set properties : Socket socket , NumberProtocolStateMachine stateMachine");
            throw new BeanCreationException(this.getClass().getName()+" must have set properties : Socket socket , NumberProtocolStateMachine stateMachine");
        }
        if (bufferSize <= 0) {
            throw new BeanCreationException("Buffer size must be greater than 0");
        }
    }

}