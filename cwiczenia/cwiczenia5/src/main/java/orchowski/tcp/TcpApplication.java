package orchowski.tcp;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import orchowski.tcp.client.Client;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import sun.misc.Signal;

import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import org.springframework.core.task.TaskRejectedException;


@SpringBootApplication
public class TcpApplication implements CommandLineRunner {
    public static final Logger logger = LoggerFactory.getLogger(TcpApplication.class);
    GenericXmlApplicationContext ctx;
    @Setter @Getter
    boolean working = true;

    public static void main(String[] args) {
        SpringApplication.run(TcpApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Signal.handle(new Signal("INT"),sig -> { working = false;});
        //Signal.handle(new Signal("TERM"),sig -> {working=false;});
        ctx = new GenericXmlApplicationContext("protocol.xml");
        var threadPool = ctx.getBean("threadPool", ThreadPoolTaskExecutor.class);
        var serverSocket = ctx.getBean("serverSocket", ServerSocket.class);
        while (working){
            Socket socket = null;
            try {
                socket = serverSocket.accept();
            } catch (SocketTimeoutException e) {
                logger.debug("Server socket timeout.... repeating serverSocket.accept()");
                continue;
            }
            var client = ctx.getBean("ClientNumberProtocol", Client.class);
            client.setSocket(socket);
            try {
                threadPool.execute(client);
            } catch (TaskRejectedException e) {
                //TODO // FIXME: 09.04.2021
                // wiadomosc wysylana do hosta w przypadku zajetego servera nie powinna byc raczej hardcodowana
                var outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
                outputStreamWriter.write("BUSY".toCharArray());
                outputStreamWriter.flush();
                outputStreamWriter.close();
                socket.close();
            }

        }
        ctx.close();
        logger.info("Closing application");
    }
}
