package orchowski.tcp.stateMachine;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import orchowski.tcp.client.Client;

import java.net.ServerSocket;

@ToString
public class StateMachineNumberProtocol extends StateMachine {
    protected int totalSum;
    protected int tempSum;
    boolean errorOccurred;
    StringBuilder builder = new StringBuilder();
    state currentState;


    public StateMachineNumberProtocol() {
        totalSum = 0;
        tempSum = 0;
        currentState = state.initial;
    }


    @Override
    public void processRequest(char[] request){
        for (char c : request) {
            // 1-9,spacje,\n,\r zostane castowane poprawnie. reszta i tak generuje blad to outputu
            changeState(c);
        }
    }

    @Override
    protected void changeState(char c){
        // initial -> *
        if(currentState == state.initial){
            if (Character.isDigit(c)) {
            // initial -> number
                tempSum+=Character.getNumericValue(c);
                currentState = state.number;
                return;
            }
            // initial -> errorOccurred
            currentState = state.error;
            errorOccurred =true;
            return;
        }
        // number -> *
        if (currentState == state.number) {
            if (Character.isDigit(c)){
            // number -> number
                tempSum *= 10;
                tempSum += Character.getNumericValue(c);
                currentState = state.number;
                return;
            }
            if (c == ' ') {
            // number -> space
                totalSum += tempSum;
                tempSum = 0;
                currentState = state.space;
                return;
            }
            if(c == '\r'){
            // number -> \r
                totalSum += tempSum;
                tempSum = 0;
                currentState = state.sr;
                return;
            }
            errorOccurred =true;
            currentState = state.error;
            return;
        }
        // space -> *
        if (currentState == state.space) {
            if(Character.isDigit(c)){
            // space->number
                tempSum = Character.getNumericValue(c);
                currentState = state.number;
                return;
            }
            // space -> else
            currentState = state.error;
            errorOccurred = true;
            return;
        }
        // error -> *
        if (currentState == state.error) {
            if (c == '\r') {
            // error -> \r
                currentState = state.sr;
                return;
            }
            errorOccurred=true;
            currentState = state.error;
            return;
        }
        // \r -> *
        if (currentState == state.sr) {
            if (c == '\n') {
            // \r -> \n
                if (errorOccurred) {
                    builder.append("ERROR\r\n");
                } else {
                    builder.append(Integer.toString(totalSum));
                    builder.append("\r\n");
                }
                currentState = state.sn;
                errorOccurred = false;
                totalSum = 0;
                return;
            }
            // \r -> error
            errorOccurred = true;
            currentState = state.error;
            return;
        }
        // \n -> *
        if (currentState == state.sn) {
            if (Character.isDigit(c)) {
            //\n -> number
                tempSum = Character.getNumericValue(c);
                currentState = state.number;
                return;
            }
            if(c == '\r'){
                // \n->\r
                // ze zmiana flagi errorOccured
                // zmiana niezgodna z zachowaniem maszyny
                errorOccurred=true;
                currentState=state.sr;
                return;
            }
            //\n -> error
            errorOccurred = true;
            currentState = state.error;
            return;
        }

    }

    @Override
    public char[] getResponse() {
        var chars = new char[builder.length()];
        builder.getChars(0,builder.length(),chars,0);
        builder = new StringBuilder();
        return chars;
    }

}

