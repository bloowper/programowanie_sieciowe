package orchowski.tcp.stateMachine;

import orchowski.tcp.client.Client;

public abstract class StateMachine {

    public abstract void processRequest(char[] request);

    abstract public char[] getResponse();

    abstract protected void changeState(char aChar);
}
