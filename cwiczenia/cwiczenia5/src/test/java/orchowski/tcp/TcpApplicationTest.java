package orchowski.tcp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;


class TcpApplicationTest {

    TcpApplication application;
    Logger logger = LoggerFactory.getLogger(TcpApplicationTest.class);
    Properties properties;
    int waitTime = 25;//Oczekiwanie watku na odpowiedz z servera, zahardcodowana bo nie za bardzo sensowne wydaje mi sie wrzucanie takich zmiennych do propertiesow
    @BeforeEach
    void init() throws IOException {
        application = new TcpApplication();
        properties = new Properties();
        try (var inputStream = new InputStreamReader(new FileInputStream("src/main/resources/protocol.properties"))) {
            properties.load(inputStream);
        }

    }

    @Test
    void clientTest1() throws InterruptedException, IOException {
        var app = new Thread(() -> {
            try {
                application.run(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        app.start();
        logger.info("Oczekiwanei na rozruch aplikacji do testo integracyjnych: 10 s\n");
        Thread.sleep(3000);
        logger.info("Rozpoczynanie testow integracyjnych");
        // WLASCIWIA CZESC TESTOW

        //TODO: 24.04.2021
        // wrzucić dodatkowy watek dla jeszcze jednego klienta ktory nie bedzie testowany ale bedzie ingenrowac z sereverm w trakcie gdy wlasciwie testy beda
        // przeprowadzane rownolegle na innym watku
        // pozwoli to zautomatyzować proces testu integracyjnego servera pracującego równolegle z wielomia klientami

        try {
            logger.info("Oczekiwania na podlaczenie socketa do aplikacji");
            var socket = new Socket("127.0.0.1", Integer.parseInt(properties.getProperty("protocol.serverSocket.port")));
            logger.info("Polaczono z aplikacja");
            var outputStream = socket.getOutputStream();
            var inputStream = socket.getInputStream();
            byte[] zapytanie;
            byte[] odpowiedzBuffer = new byte[Integer.parseInt(properties.getProperty("protocol.bufferSize"))];
            byte[] oczekiwanaOdpowiedz;
            int nOfReadBytes;


            logger.debug("Test fragment 1: START");
            zapytanie = "12 12 12\r\n".getBytes(StandardCharsets.US_ASCII);
            oczekiwanaOdpowiedz = "36\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 1: END");


            logger.debug("Test fragment 2: START");
            zapytanie = "1 1 1 1 1 1 1 1 1 1\r\n".getBytes(StandardCharsets.US_ASCII);
            oczekiwanaOdpowiedz = "10\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 2: END");

            logger.debug("Test fragment 3: START");
            zapytanie = "1 1 1 1 1 1 1 1 1 1\r\n2 2 2\r\n1 1 1 1\r\n".getBytes(StandardCharsets.US_ASCII);
            oczekiwanaOdpowiedz = "10\r\n6\r\n4\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 3: END");


            logger.debug("Test fragment 4: START");
            zapytanie = "1 1 1 1 1 1 1 1f 1 1\r\n2  2 2\r\n1 1 1 1\r\n".getBytes(StandardCharsets.US_ASCII);
            oczekiwanaOdpowiedz = "ERROR\r\nERROR\r\n4\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 4: END");


            logger.debug("Test fragment 5: START");
            zapytanie = "\r\n\r\n\r\n".getBytes(StandardCharsets.US_ASCII);
            oczekiwanaOdpowiedz = "ERROR\r\nERROR\r\nERROR\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 5: END");


            logger.debug("Test fragment 6: START");
            zapytanie = "100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            oczekiwanaOdpowiedz = "900\r\n".getBytes(StandardCharsets.US_ASCII);
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 6: END");


            logger.debug("Test fragment 7: START");
            zapytanie = " 100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            oczekiwanaOdpowiedz = "ERROR\r\n".getBytes(StandardCharsets.US_ASCII);
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 7: END");


            logger.debug("Test fragment 8: START");
            zapytanie = "100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100 ".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            zapytanie = "100 100 100a\r\n".getBytes(StandardCharsets.US_ASCII);
            outputStream.write(zapytanie);
            outputStream.flush();
            oczekiwanaOdpowiedz = "ERROR\r\n".getBytes(StandardCharsets.US_ASCII);
            Thread.sleep(waitTime);
            nOfReadBytes = inputStream.read(odpowiedzBuffer);
            assertArrayEquals(oczekiwanaOdpowiedz, Arrays.copyOfRange(odpowiedzBuffer,0,nOfReadBytes));
            logger.debug("Test fragment 8 : END");

        } finally {
            logger.info("Zamykanie aplikacji po testach integracyjnych: 1s\n");
            Thread.sleep(1000);
            application.setWorking(false);
            app.join();
            logger.info("Aplikacja po testach zostala zamknieta");
        }


    }
}