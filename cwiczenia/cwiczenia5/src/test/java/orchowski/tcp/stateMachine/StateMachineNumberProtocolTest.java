package orchowski.tcp.stateMachine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class StateMachineNumberProtocolTest {

    StateMachineNumberProtocol protocol;

    @BeforeEach
    void init() {
        protocol = new StateMachineNumberProtocol();
    }

    @Test
    void changeState() {
        assertEquals(state.initial, protocol.currentState);
        protocol.changeState('1');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('2');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('3');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('4');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('5');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('6');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('7');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('8');
        assertEquals(state.number, protocol.currentState);

        protocol.changeState(' ');
        assertEquals(state.space,protocol.currentState);

        protocol.changeState('1');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('3');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('5');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('6');
        assertEquals(state.number, protocol.currentState);

        protocol.changeState('\r');
        assertEquals(state.sr, protocol.currentState);
        protocol.changeState('\n');
        assertEquals(state.sn, protocol.currentState);

        protocol.changeState('1');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('3');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('5');
        assertEquals(state.number, protocol.currentState);
        protocol.changeState('6');
        assertEquals(state.number, protocol.currentState);

        protocol.changeState('\n');
        assertEquals(state.error,protocol.currentState);
        protocol.changeState('\r');
        assertEquals(state.sr,protocol.currentState);
        protocol.changeState('\n');
        assertEquals(state.sn,protocol.currentState);

    }

    @Test
    void getResponse() {
        var request = "123\r\n".toString().toCharArray();
        var expected = "123\r\n".toString().toCharArray();
        protocol.processRequest(request);
        var response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "1 1 1\r\n".toCharArray();
        expected = "3\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "125 125\r\n".toCharArray();
        expected = "250\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "125. 125\r\n".toCharArray();
        expected = "ERROR\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "125 125\r\n".toCharArray();
        expected = "250\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "\r\n".toCharArray();
        expected = "ERROR\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "\r\n".toCharArray();
        expected = "ERROR\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "100 100 100 100 100 100 100 100 100 100\r\n".toCharArray();
        expected = "1000\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = " 100 100 100 100 100 100 100 100 100 100\r\n".toCharArray();
        expected = "ERROR\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "100 100 100 100 100 100 100 100 100 100\r\n".toCharArray();
        expected = "1000\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        request = "125 25\r\n25 125\r\n".toCharArray();
        expected = "150\r\n150\r\n".toCharArray();
        protocol.processRequest(request);
        response = protocol.getResponse();
        assertArrayEquals(expected,response);

        protocol.processRequest("125 25".toCharArray());
        protocol.processRequest("\r\n".toCharArray());
        protocol.processRequest("125 25".toCharArray());
        protocol.processRequest("\r\n".toCharArray());
        expected = "150\r\n150\r\n".toCharArray();

    }
}