#tcp
## komendy
* `./gradlew bootRun` pobranie zaleznosci->kompilacja->uruchomienie testow->uruchomienie projektu z folderu projektu
* `./gradlew build` zbuodowanie projektu/utworzenie wykonywalnego jara wykonywanlego poprzez `java -jar (nazwa jara)`\
jar bedzie znajdowac sie w `/build/libs`
* `./gradlew clean` usiniecie folderu build
* `./gradlew test` uruchomienie samych testow
* `./gradlew tasks` reszta komend w projekcie
###ustawienia aplikacji
### `resources/protocol.properties` zawieraja
* `protocol.bufferSize` rozmiar buffora przyjmowany przez gniazdka rozmawiajace z klientami
* `protocol.serverSocket.port`    port na ktorym aplikacja nasluchuje nowych klientow
* `protocol.serverSocket.timeout` timeout dla gniazdka nasluchujacego, mozna ustawic na 0 wtedy timeout jest wylaczony\
  (uzywane do przerywania pracy aplikacji przy uzyciu kodu INT/ctrl+c)
* `threadPool.corePoolSize` podstawowa(minimalna) ilosc wątków do obsługi klientów
* `threadPool.maxPoolSize` maxymalna ilosc watkow do obslugi klientow
* `threadPool.queueCapicity`     maxymalna ilosc klientow oczekujacych na obsluzenie przez watek, odpowiedz\
  na ich zapytania zostanie dopiero wygenerowana i odeslana gdy klient dostanie czas w poolThread, w przypadku\
  zapelnienia tej kolejki polaczenie z nowym klientem zostanie przerwane. zostanie wygenerowana stosowna odpowiedzia\
  oznaczajaca zajety serwer
###`resources/application.properties` zawierają
* logging.level.root = info
* logging.level.orchowski.tcp = trace\
sa to ustawienia związane z logami generowanymi przez aplikacje\
  obecnie sa ustawione zeby wyswietlac wszystie mozliwe informacje zwizane z pakietem\
  `orchowski.tcp` i zadnych dodatkowych zwiazanych z samym kontenerem springa
### pierwsze wywolanie ./gradlew build lub ./gradlew bootRun
bedzie trwac wzglednie dlugo: 
* 1 zostaną pobrane zależności do projektu zdefiniowane w build.gradle depedencies (jest ich 4 + junit/dbunit/mocito(testowe))
* 2 projekt zostanie zbudowany
* 3 zostane wykonane wszystkie testy jednostkowe zalaczone do projektu znajdujace sie w `src/test/*`


## aktualne ustawienia
* threadPool.corePoolSize = 10
* threadPool.maxPoolSize = 10
* threadPool.queueCapacity = 10\
czyli aktualnie moze byc obslugiwanych 10 klientow jednoczesnie, a kolejnych 10 bedzie oczekiwac w kolejce,
  ich zaptyania zodstana przetworzone dopiero do dostaniu czasu w ThradPool'u
## ps
zamysl byl taki zeby w projekcie wartosci nie bylo hardcodowane tylko  pobierane z odpowiednich .properties i odpowiednio injectiowane 
do projektu poprzez wzorzec depedency injection. Ale z racji tego ze to dopiero moj poczatek nauki takiej bardziej sensowniejszej javy
to wyszlo to dosyc pokracznie wiec chcąc napisać kod jak najbardziej nastawiony na potencjalne zmiany i bedacy jak najpardizej low coupled
osiagnalem chyba cos przeciwnego\
**nastepny projekt napisze już bez kontenera realizującego wzorzec ioc - springa**

